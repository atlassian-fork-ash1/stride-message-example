/**
 * This module is used to request a valid access token to make API requests.
 * We're getting our environment variables which hold our client_id and client_secret for our app.
 * We pass that into the data when making our request.
 * Our function will return the accessToken to be used elsewhere.
 */

require('dotenv').config()
const debug = require('debug')('stride:accessToken')

/* from the documentation, how to request an access token

   curl --request POST \
    --url 'https://api.atlassian.com/oauth/token' \
    --header 'content-type: application/json' \
    --data '{"grant_type":"client_credentials","client_id": "{clientId}", "client_secret": "{clientSecret}"}'

  https://developer.atlassian.com/cloud/stride/security/authentication/
*/
async function accessToken(axios, envProcess) {
  //lets inject our dependencies so we can easily run tests with mocks 🤹‍
  axios = axios || require('axios')
  // eslint-disable-next-line no-global-assign
  process = envProcess || process

  debug('Requesting access token.')
  return await axios({
    method: 'post',
    url: 'https://api.atlassian.com/oauth/token',
    data: {
      grant_type: 'client_credentials',
      client_id: process.env.CLIENT_ID,
      client_secret: process.env.CLIENT_SECRET
    }
  })
}

module.exports = accessToken
