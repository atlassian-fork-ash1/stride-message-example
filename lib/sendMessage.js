/**
 * This module is used to send a simple text message into a conversation.
 * We're using the axios (https://www.npmjs.com/package/axios) module which is a
 * Promise based HTTP client to make requests.
 *
 * First we need an access token.  We call over to the accessToken module to get an access token
 * We can then use that access token to authorize the post of our message to the conversation
 */

const accessToken = require('./accessToken')
const debug = require('debug')('stride:sendMessage')

async function sendMessage(msg, context, axios, token) {
  // injecting our dependency for testing
  axios = axios || require('axios')

  /**
   * If we're passing in a token (from our tests) use that, if not get one.
   * Call the accessToken module which makes a call to get a valid token
   * We're using async/await so that this request finishes first before moving on
   */
  token = token
    ? token
    : await accessToken(axios, process).then(res => res.data)

  debug('Access token:', token)

  /**
   * Here we're going to post to our site/conversation that was passed to us in the context
   * We're only creating a plain text message in the room.
   */
  return axios({
    method: 'POST',
    url:
      'https://api.atlassian.com/site/' +
      context.cloudId +
      '/conversation/' +
      context.conversationId +
      '/message',
    headers: {
      authorization: `Bearer ${token.access_token}`,
      'cache-control': 'no-cache',
      'content-type': 'text/plain'
    },
    data: msg
  })
    .then(res => {
      debug('Message sent!', res.data)
      return res.data
    })
    .catch(err => debug('Error received:', err))
}

module.exports = sendMessage
