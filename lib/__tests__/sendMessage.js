const sendMessage = require('../sendMessage')

describe('Send a message', () => {
  it('We sent a message', () => {
    const mockContext = {
      cloudId: 'fake123cloud',
      userId: 'fake123user',
      conversationId: 'fake123conversation'
    }

    const mockMessage = jest.fn().mockReturnValue(
      Promise.resolve({
        data: {
          id: 'fakeMessageId'
        }
      })
    )

    const mockToken = {
      access_token: 'xxx',
      expires_in: 3600,
      scope: 'participate:conversation',
      token_type: 'Bearer'
    }

    return sendMessage(
      'This is a test',
      mockContext,
      mockMessage,
      mockToken
    ).then(res => {
      expect(res.id).toBe('fakeMessageId')
      expect(mockMessage).toBeCalledWith({
        method: 'POST',
        url:
          'https://api.atlassian.com/site/' +
          mockContext.cloudId +
          '/conversation/' +
          mockContext.conversationId +
          '/message',
        headers: {
          authorization: `Bearer ${mockToken.access_token}`,
          'cache-control': 'no-cache',
          'content-type': 'text/plain'
        },
        data: 'This is a test'
      })
    })
  })
})
